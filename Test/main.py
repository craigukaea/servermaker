from ClassMaker import ClassMaker
from opcua import Server

class ServerMaker:
    __classMaker = ClassMaker()
    
    def __init__(self):
        self.__classMaker.setClassName('Server')
        self.__classMaker.addVariable('server', Server())
        self.__classMaker.addVariable('idx', 5)
        self.__classMaker.addFunctionToInitialiser(lambda cself : cself.server.set_endpoint("opc.tcp://0.0.0.0:4849/freeopcua/server/"))


    #ToDo objectName should be a path, and folders will be created
    def addobject(self, objectName):
        #add to the address space
        self.__classMaker.addFunctionToInitialiser(lambda cself : print("Adding object" + objectName))
        self.__classMaker.addFunctionToInitialiser(lambda cself : cself.server.get_objects_node().add_object(cself.idx, objectName))

    def addMethod(self, methodName, callable):
        #Add to the address space
        self.__classMaker.addFunctionToInitialiser(lambda cself : cself.server.get_objects_node().add_method(cself.idx, methodName, callable))
        
    def makeServer(self):
        #start the server at the end of the initialiser
        self.__classMaker.addFunctionToInitialiser(lambda cself : print("Starting Server!"))
        self.__classMaker.addFunctionToInitialiser(lambda cself : cself.server.start())
        return self.__classMaker.makeClass()
    
    
if __name__== "__main__":
    
    maker = ServerMaker()
    
    maker.addobject("Foo")
    maker.addMethod("Bar", lambda x: print("Hello"))
    
    server = maker.makeServer()
    
    server.__init__(server)
    
    while True:
        pass