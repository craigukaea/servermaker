import setuptools
with open("README.md", "r") as fh:
    long_description = fh.read()
    
setuptools.setup(
     name='ServerMaker',  
     version='0.1',
     author="Craig Hickman",
     author_email="craig.hickman@ukaea.uk",
     description="A helper class to build opc-ua servers",
     long_description=long_description,
     long_description_content_type="text/markdown",
     url="https://gitlab.com/craigukaea/objterm",
     packages=setuptools.find_packages(),
     install_requires=[
         'opcua',
         'ClassMaker'
    ],
     classifiers=[
         "Programming Language :: Python :: 3",
         "License :: OSI Approved :: MIT License",
         "Operating System :: OS Independent",
     ]
 )
