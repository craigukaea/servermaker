
### Use

Configure the server maker with:
```
maker.addobject(<objectName>)
```

and,
```
maker.addMethod(<methodName>, <functionToCall>)
```

This will add objects and methods to the server that can be built with:

```
server = maker.makeServer()
```

### Example

```python
if __name__== "__main__":
    
    maker = ServerMaker()
    
    maker.addobject("Foo")
    maker.addMethod("Bar", lambda x: print("Hello"))
    
    server = maker.makeServer()
    
    # This server will have an object "Foo", and a method "Bar" taht prints "Hello"
    server.__init__(server)
    
    while True:
        pass
```
